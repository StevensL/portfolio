/*
Page contenant les informations du components pour la page Projet2
avec des image de du projet
*/

import Biblio from '../components/Biblio';
import Library from '../img/Biblio.jpg';
import Login from '../img/Login.png';
import styles from './Projet2.module.css';

export default function Projet2() {
    return <div className={styles.projet2}>
        
        <div className={styles.images2}>
            <img src={Library} alt="Accueil du Restaurant" className={styles.biblioAccueil}/>
            <img src={Login} alt="Menu du Restaurant" className={styles.login} />
        </div>

        <Biblio />
    </div>
}