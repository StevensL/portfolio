/*
Page contenant les informations du components pour la page Projet1
avec des image de du projet
*/

import Accueil from '../img/Restaurant.png';
import Menu from '../img/Menu.png';
import styles from './Projet1.module.css';
import Restaurant from '../components/Restaurant';

export default function Projet1() {
    return <div className={styles.projet1}>

        <Restaurant />
        
        <div className={styles.images}>
            <img src={Accueil} alt="Accueil du Restaurant" />
            <img src={Menu} alt="Menu du Restaurant" className={styles.menu} />
        </div>
    </div>
}