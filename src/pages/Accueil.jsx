/*
Page contenant les informations du components pour la page d'Accueil
avec une image de profile
*/

import EducationSkills from '../components/EducationSkills';
import ProfilePic from '../img/Portfolio.jpg';

import styles from './Accueil.module.css';

export default function Accueil() {
    return <div className={styles.accueil}>
        
        <img src={ProfilePic} alt="My profile" className={styles.photoProfile}/>

        <div className={styles.titres}>
            <h2><span>Hi, I'm</span> Stevens Lucien</h2>
            <h3>Computer Programmer</h3>
        </div>
        
        <EducationSkills />

        <div className={styles.info}>
            <p>Contact Number: (613) 581-1244</p>
            <p>Contact Email: stevens199@hotmail.ca</p>
        </div>
    </div>
}