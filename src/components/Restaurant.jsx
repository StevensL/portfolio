/*
Page contenant les explication du Projet1 Restaurant.
*/

import styles from './Restaurant.module.css';

export default function Restaurant() {
    return <div className={styles.restaurant}>
    <h2 className={styles.menuTitre}>
        Restaurant
    </h2>
    <p>
        For this project, we were able to create a website based on 
        a fictional restaurant. This restaurant contained multiple pages such as the
        Home Page, the menu page, a shopping cart page, orders page, login and inscription page,
        and finally a about us page which directly connected to a contact us page.
    </p>
    <p>
        To realise this project we used Visual Studio, it gave us the opportunity to
        use HTML, CSS and JavaScript. Furthermore, this project required a SQL database to
        contain a multitude of important information for our website.
    </p>
</div>
}