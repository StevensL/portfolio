/*
Page contenant les explication du Projet2 Bibliothèque,
*/

import styles from './Biblio.module.css';

export default function Biblio() {
    return <div className={styles.biblio}>
    <h2 className={styles.biblioTitre}>
        Bibliothèque
    </h2>
    <p>
        For this project, in which my colleagues and I are currently creating, our main objective is
        to create a Library website that gives you the opportunity to order books by either
        renting them or buying them. Users should be able to search for the existing books and/or go through
        the home page to gain access to the book categories. But they shouldn't be able to order anything without having an account.
    </p>
    <p>
        To realise this project we used Visual Studio, it gave us the opportunity to
        use HTML, CSS and JavaScript. Furthermore, this project required a SQL database to
        contain a multitude of important information for our website.
    </p>
</div>
}