/*
Applique l'opportunité de rendre visible ou 
invisible certains oppération en appuyant sur une tâche,
*/

import { useState } from 'react';

export default function CvIntro (props) {
    const [visible, setVisible] = useState(!!props.visible);
    
    const toggleContent = () => {
            setVisible(!visible);
    }

    return <>
        <div onClick={toggleContent}>
                { props.title }
        </div>

        {visible &&
                <div>
                        { props.children }   
                </div>
        }
    </>
}