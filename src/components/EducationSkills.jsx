/*
Page contenant les informations relié sous Education et Skills de la page d'accueil.
Utilise le toggle du CvIntro pour fermer et ouvire ces informations.
*/

import styles from './EducationSkills.module.css';
import CvIntro from './CvIntro';

export default function EducationSkills() {
    return <div className={styles.cvIntro}>
    
    <div className={styles.introduction}>
        <h3 className={styles.welcome}>Welcome to My Portfolio!</h3>
        <p>I am 22 years old! And here I will be providing my resume and projects </p>
    </div>
   
    <div className={styles.education}>
        <CvIntro title="Education">
            <p className={styles.elements}>
                    2016-2017       Graduated from École Secondaire Catholique Béatrice Desloges
            </p>
            <p className={styles.elements}>
                    2017-2018       Ottawa University : Studying in Communication and Political Science
            </p>
            <p className={styles.elements}>
                    2019-Present    Cité Collegiale : Currently studying in computer Programming
            </p>
        </CvIntro>
    </div>

    <div className={styles.skills}>
        <CvIntro title="Computer Skills" >
            <p className={styles.elements}>
                HTML CSS JavaScript C# Java SQL
            </p>
        </CvIntro>
    </div>
</div>
}

