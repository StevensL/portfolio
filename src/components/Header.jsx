/*
Page Header contenant le MenuNav et le titre,
*/

import MenuNav from './MenuNav';
import styles from './Header.module.css';

export default function Header(props) {
    return <header className={styles.header}>
        <h1>Portfolio</h1>
        <MenuNav changePage={props.changePage} />
    </header>
}