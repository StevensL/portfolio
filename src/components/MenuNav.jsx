/*
Page contenant à l'entête les noms de pages avec l'option changepage
onclick pour changer de page.
*/

import styles from './MenuNav.module.css';

export default function MenuNav(props) {
    return <nav>
        <ul className={styles.list}>
            <li>
                <button onClick={props.changePage('accueil')}>Home</button>
            </li>
            <li>
                <button onClick={props.changePage('projet1')}>Project 1</button>
            </li>
            <li>
                <button onClick={props.changePage('projet2')}>Project 2</button>
            </li>
        </ul>
    </nav>
}
